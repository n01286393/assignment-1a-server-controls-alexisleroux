﻿<%@ Page Language="C#" Inherits="http5101_a1.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Default</title>
</head>
<body>
    <form action="Default.aspx" method="post" runat="server">
           <h1>Pizza Delivery Service</h1> 
            <fieldset>
                <legend>General Personal Information</legend>
                
                <asp:Label id="FullName" runat="server" Text="Full Name"></asp:Label>
                <asp:TextBox id="nameText" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="name" runat="server" ControlToValidate="nameText"   
ErrorMessage="Please enter your full name" ForeColor="Red">*</asp:RequiredFieldValidator>  
                <br>
                
                <asp:Label id="phoneNum" runat="server" Text="Phone Number"></asp:Label>
                <asp:TextBox id="phoneText" runat="server" placeholder="e.g.(647)-999-9999"/>
                
                <br>
                
                <asp:Label id="email" runat="server" Text="Email"></asp:Label>
                <asp:TextBox id="emailText" runat="server"></asp:TextBox>
                
                <br>
                
                <asp:Label id="unitNum" runat="server" Text="Unit Number"></asp:Label>
                <asp:TextBox id="unitNumText" runat="server"></asp:TextBox>
                
                <asp:Label id="streetNum" runat="server" Text="Street Number"></asp:Label>
                <asp:TextBox id="streetNumText" runat="server"></asp:TextBox>
                
                <br>
                
                <asp:Label id="street" runat="server" Text="Street"></asp:Label>
                <asp:TextBox id="streetText" runat="server"></asp:TextBox>
                
                <br>
                
                <asp:Label id="postalCode" runat="server" Text="Postal Code"></asp:Label>
                <asp:TextBox id="postalCodeText" runat="server"></asp:TextBox>
                
                <br>
                
                <asp:Label id="confirmationLabel" runat="server" Text="Confirmation"></asp:Label>
                <br>
                <asp:RadioButton GroupName="confirmation" runat="server" Text="Check to Receive Confirmation via Email"></asp:RadioButton><br>
                <asp:RadioButton GroupName="confirmation" runat="server" Text="Check to Receive Confirmation via SMS"></asp:RadioButton> 
                
                <fieldset>
                    <legend>Pizza Order</legend>
                    
                    <asp:Label id="topping" runat="server" Text="Pizza Topping(s):"></asp:Label>
                    <br>
                    <asp:CheckBox id="pepperoni" runat="server" Text="Pepperoni"></asp:CheckBox>
                    <asp:CheckBox id="sausage" runat="server" Text="Sausage"></asp:CheckBox>
                    <asp:CheckBox id="mushrooms" runat="server" Text="Mushrooms"></asp:CheckBox>
                    <asp:CheckBox id="peppers" runat="server" Text="Peppers"></asp:CheckBox>
                    <asp:CheckBox id="tomatoes" runat="server" Text="Tomatoes"></asp:CheckBox>
                    <asp:CheckBox id="onions" runat="server" Text="Onions"></asp:CheckBox>
                    
                    <br>
                    <asp:Label id="sauce" runat="server" Text="Pizza Sauce"></asp:Label>
                    <br>
                    <asp:DropDownList id="sauceDropDown" runat="server">
                        <asp:ListItem Value="">Please Select</asp:ListItem>
                        <asp:ListItem>Tomato</asp:ListItem>
                        <asp:ListItem>Tomato Herb</asp:ListItem>
                        <asp:ListItem>Bolognese</asp:ListItem>
                        <asp:ListItem>Pesto</asp:ListItem>
                    </asp:DropDownList>
                    
                    <br>
                    
                    <asp:Label id="deliveryDetails" runat="server" Text="Delivery Instructions:"></asp:Label><br>
                    <asp:TextBox id="deliveryDetailsText" runat="server" width="500px" placeholder="i.e. Watch out for the dog in the front"></asp:TextBox>
                    
                </fieldset>
                
                <asp:Button id="orderButton" runat="server" Text="Order"></asp:Button>
                
                <asp:ValidationSummary id="ValidationSummary1" runat="server" ForeColor="Red"></asp:ValidationSummary>
        </form>
</body>
</html>

<!---


- A regular expression validator
- A compare validator or a range validator -->