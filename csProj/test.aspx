﻿<%@ Page Language="C#" EnableEventValidation="false"%>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>test</title>
	<script runat="server">
	
	</script>
</head>
<body>
	<form id="form1" runat="server">
        <h1>MyHumber Registration:</h1> 
            <fieldset>
                <legend>General Personal Information:</legend>
                
                <asp:Label runat="server" Text="Full Name"></asp:Label>
                <asp:TextBox id="fullNameInput" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="fullNameValidator" runat="server" ControlToValidate="fullNameInput"   
ErrorMessage="Please enter your full name" ForeColor="Red"/>  
                <br>
                
                <asp:Label runat="server" Text="Phone Number"></asp:Label>
                <asp:TextBox id="phoneNumberInput" runat="server" placeholder="e.g.(647)-999-9999"/><br>
                <asp:RequiredFieldValidator ID="phoneNumberValidator" runat="server" ControlToValidate="phoneNumberInput"   
ErrorMessage="Please enter your phone number" type="integer" ForeColor="Red"/>  
                
                <br>
                
                <asp:Label runat="server" Text="Student Number"></asp:Label>
                <asp:TextBox id="studentNumberInput" runat="server" placeholder="e.g.N12345678"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="studentNumberValidator" runat="server" ControlToValidate="studentNumberInput"   
ErrorMessage="Please enter your student number" ForeColor="Red"/><br>
                
                <asp:Label runat="server" Text="Email"></asp:Label>
                <asp:TextBox id="emailInput" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="emailInputValidator" runat="server" ControlToValidate="emailInput"   
ErrorMessage="Please enter your preferred email number" type="integer" ForeColor="Red"/><br>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="emailInput"   
ErrorMessage="Sorry, that is not a valid email" ForeColor="Red"ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"/>
                
                <br>
                
                <asp:Label runat="server" Text="Please Retype Email:"></asp:Label>
                <asp:TextBox id="emailInputRetype" runat="server"></asp:TextBox><br>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="emailInput"   
ControlToValidate="emailInputRetype" Display="Dynamic" ErrorMessage="Please center the correct email" ForeColor="Red"   
Operator="Equal" Type="String"/>
                
                <br>
                
                <asp:Label runat="server" Text="Confirmation"></asp:Label>
                <br>
                <asp:RadioButton GroupName="confirmationOrderButton" runat="server" Text="Check to Receive Humber Notifications via Email"></asp:RadioButton><br>
                <asp:RadioButton GroupName="confirmationOrderButton" runat="server" Text="Check to Receive Humber Notifications via SMS"></asp:RadioButton><br> 
           
            </fieldset>
            
            <fieldset>
                <legend>Housing Information</legend>
                <asp:Label runat="server" Text="What kind of unit do you live in?"></asp:Label><br>
                <asp:DropDownList id="residenceList" runat="server">
                    <asp:ListItem Value="">Please Select</asp:ListItem>
                    <asp:ListItem>Condomonium</asp:ListItem>
                    <asp:ListItem>Humber Residence</asp:ListItem>
                    <asp:ListItem>Detached House</asp:ListItem>
                    <asp:ListItem>Townhouse</asp:ListItem>
                    <asp:ListItem>Duplex/Triplex</asp:ListItem>
                </asp:DropDownList><br>
                
                <br>
                <asp:Label runat="server" Text="Street Number"></asp:Label>
                <asp:TextBox id="streetNumInput" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="streetNumValidator" runat="server" ControlToValidate="streetNumInput"   
ErrorMessage="Please enter your home number" type="integer" ForeColor="Red"/>  
                
                <br>
                
                <asp:Label runat="server" Text="Street"></asp:Label>
                <asp:TextBox id="streetInput" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="streetInputValidator" runat="server" ControlToValidate="streetInput"   
ErrorMessage="Please enter your street name" ForeColor="Red"/>  
                
                <br>
                
                <asp:Label runat="server" Text="Postal Code"></asp:Label>
                <asp:TextBox id="postalCodeInput" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="postalCodeValidator" runat="server" ControlToValidate="postalCodeInput"   
ErrorMessage="Please enter your postal code" ForeColor="Red"/><br>
                <asp:RegularExpressionValidator ID="postalCodeRegularExpressionValidator" runat="server" ControlToValidate="postalCodeInput"   
ErrorMessage="Sorry, that is not a valid postal code" ForeColor="Red" ValidationExpression="[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]"/>
                
                <br>
                 <asp:Label id="transporation" runat="server" Text="Method(s) of transportation that you use to travel to Humber:"></asp:Label><br>
                    <asp:CheckBox id="subway" runat="server" Text="Subway"></asp:CheckBox>
                    <asp:CheckBox id="bus" runat="server" Text="Bus"></asp:CheckBox>
                    <asp:CheckBox id="bike" runat="server" Text="Bike"></asp:CheckBox>
                    <asp:CheckBox id="walk" runat="server" Text="Walk"></asp:CheckBox>
                    <asp:CheckBox id="car" runat="server" Text="Car"></asp:CheckBox>
                    <asp:CheckBox id="train" runat="server" Text="Train"></asp:CheckBox>
                    
                <br>
         </fieldset>

                <asp:Button id="submitButton" runat="server" Text="Submit"></asp:Button>
                
                <asp:ValidationSummary id="ValidationSummary" runat="server" ForeColor="Blue"></asp:ValidationSummary>
        </form>
</body>
</html>

<!--Regular Expression for Email found at:
https://www.javatpoint.com/asp-net-web-form-regular-expression-validator

Regular Expression for Postal Code found at:
https://stackoverflow.com/questions/1146202/canadian-postal-code-validation 
-->